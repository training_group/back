const dropCollection = require("../../../src/models/db/dropCollection");
const writeData = require("../../../src/models/db/writeData");
const getCollectionNames = require("../../../src/models/db/getCollectionNames");

function test(assert, api) {
  describe("Drop the collection from the database, removing it permanently.", () => {
    let client = {};
    let db = {};
    const collectionName = "pointLayerTEST";
    const documents = [
      {
        properties: { code: "VS010-0000023-001-1019", flag: "testing" },
        geometry: {
          type: "Point",
          coordinates: [30.45937343620047, 59.724974062303296]
        }
      },
      {
        properties: { code: "VS010-0000023-001-1021", flag: "testing" },
        geometry: {
          type: "Point",
          coordinates: [30.4583300564727, 59.72510252474582]
        }
      }
    ];
    before(function before(done) {
      api.db
        .open({})
        .then(data => {
          client = data.client;
          db = data.db;
        })
        .then(() => {
          return writeData({ db, collectionName, documents }, api);
        })
        .then(res => {
          console.log(`Was write ${res.data.length} documents`);
          done();
        })
        .catch(err => {
          console.log(err);
          done();
        });
    });

    it("Success drop collection", done => {
      const params = {
        db,
        collectionName
      };
      dropCollection(params, api)
        .then(response => {
          assert.typeOf(response, "object");
          assert.isTrue(response.status, "");
          assert.equal(
            response.statusText,
            `Коллекция ${collectionName} удалена`
          );

          done();
        })
        .catch(err => console.log(err));
    });

    after(function after(done) {
      const params = {
        db
      };
      getCollectionNames(params, api)
        .then(data => {
          console.log(data.data.map(collection => collection.name));
          return api.db.close({ client });
        })
        .then(() => {
          console.log("Success close connection");
          done();
        })
        .catch(err => {
          console.log(err);
          done();
        });
    });
  });
}

module.exports = test;
