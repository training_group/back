const createIndex = require("../../../src/models/db/createIndex");
const writeData = require("../../../src/models/db/writeData");
const dropCollection = require("../../../src/models/db/dropCollection");
const intersections = require("../../../src/models/db/intersections");

function test(assert, api) {
  describe("Intersection geometry", () => {
    let client = {};
    let db = {};
    const collectionName = "testPolygon";
    const documents = [
      {
        type: "Feature",
        id: 0,
        geometry: {
          type: "Polygon",
          coordinates: [
            [
              [30.065009389648377, 59.94205169496971],
              [30.121314321289013, 59.80050227957117],
              [30.479743276367138, 59.81779678563227],
              [30.457770620117124, 59.95720646085701],
              [30.065009389648377, 59.94205169496971]
            ]
          ]
        },
        properties: {
          description: "3",
          flag: "testing"
        }
      },
      {
        type: "Feature",
        id: 1,
        geometry: {
          type: "Polygon",
          coordinates: [
            [
              [30.253150258789002, 60.01569367502241],
              [30.305335317382742, 59.884814277988426],
              [30.645911489257745, 59.87445959839377],
              [30.569007192382745, 60.01500619286085],
              [30.253150258789002, 60.01569367502241]
            ]
          ]
        },
        properties: {
          description: "2",
          flag: "testing"
        }
      },
      {
        type: "Feature",
        id: 2,
        geometry: {
          type: "Polygon",
          coordinates: [
            [
              [29.723059926757752, 60.08917142557317],
              [29.806830678710867, 59.995062969429846],
              [30.085608754882745, 60.05897614126107],
              [29.723059926757752, 60.08917142557317]
            ]
          ]
        },
        properties: {
          description: "1",
          flag: "testing"
        }
      }
    ];

    /** 3 элемент массива не пересекается со 2 и 1,
     *  1 и 2 пересекаются
     *  при отправке на проверку в слой объекта из набора, представленного в массиве documents
     *  вернется значение минимум 1
     *  * */

    before(function before(done) {
      api.db
        .open({})
        .then(data => {
          client = data.client;
          db = data.db;
        })
        .then(() => {
          return writeData({ db, collectionName, documents }, api);
        })
        .then(res => {
          console.log(`Was write ${res.data.length} documents`);
          return createIndex(
            {
              db,
              collectionName,
              query: { geometry: "2dsphere" }
            },
            api
          );
        })
        .then(res => {
          console.log(`Was created index ${res.data}`);
          done();
        })
        .catch(err => {
          console.log(err);
          done();
        });
    });

    it("Success find intersection", done => {
      const params = {
        db,
        collectionName,
        locationField: "geometry",
        targetObject: documents[2].geometry
      };
      intersections(params, api)
        .then(response => {
          assert.typeOf(response, "object");
          assert.isTrue(response.status, "");
          assert.equal(response.statusText, `Пересекаемые объекты`);
          assert.isArray(response.data);
          assert.equal(response.data.length, 1);

          done();
        })
        .catch(err => console.log(err));
    });

    it("Doesn't have intersections", done => {
      const params = {
        db,
        collectionName,
        locationField: "geometry",
        targetObject: documents[0].geometry
      };
      intersections(params, api)
        .then(response => {
          assert.typeOf(response, "object");
          assert.isTrue(response.status, "");
          assert.equal(response.statusText, `Пересекаемые объекты`);
          assert.isArray(response.data);
          assert.equal(response.data.length, 2);

          done();
        })
        .catch(err => console.log(err));
    });

    after(function after(done) {
      const params = {
        db,
        collectionName
      };
      dropCollection(params, api)
        .then(() => {
          console.log(`Was delete ${params.collectionName} collection`);
          return api.db.close({ client });
        })
        .then(() => {
          console.log("Success close connection");
          done();
        })
        .catch(err => {
          console.log(err);
          done();
        });
    });
  });

  describe("Intersection polygons", () => {
    let client = {};
    let db = {};
    const collectionName = "testPolygon";
    const documents = [
      {
        type: "Feature",
        properties: {
          desript: 1
        },
        geometry: {
          type: "Polygon",
          coordinates: [
            [
              [-120, 60],
              [120, 60],
              [120, -60],
              [-120, -60],
              [-120, 60]
            ],
            [
              [-60, 30],
              [60, 30],
              [60, -30],
              [-60, -30],
              [-60, 30]
            ]
          ]
        }
      }
    ];

    const targetObject = {
      type: "Feature",
      properties: {},
      geometry: {
        type: "Polygon",
        coordinates: [
          [
            [-50, 20],
            [50, 20],
            [50, -20],
            [-50, -20],
            [-50, 20]
          ]
        ]
      }
    };
    before(function before(done) {
      api.db
        .open({})
        .then(data => {
          client = data.client;
          db = data.db;
        })
        .then(() => {
          return writeData({ db, collectionName, documents }, api);
        })
        .then(res => {
          console.log(`Was write ${res.data.length} documents`);
          return createIndex(
            {
              db,
              collectionName,
              query: { geometry: "2dsphere" }
            },
            api
          );
        })
        .then(res => {
          console.log(`Was created index ${res.data}`);
          done();
        })
        .catch(err => {
          console.log(err);
          done();
        });
    });

    it("Success find intersection", done => {
      const params = {
        db,
        collectionName,
        locationField: "geometry",
        targetObject: targetObject.geometry
      };
      intersections(params, api)
        .then(response => {
          console.log(response.data);
          assert.typeOf(response, "object");
          assert.isTrue(response.status, "");
          assert.equal(response.statusText, `Пересекаемые объекты`);
          assert.isArray(response.data);
          assert.equal(response.data.length, 0);
          done();
        })
        .catch(err => console.log(err));
    });

    after(function after(done) {
      const params = {
        db,
        collectionName
      };
      dropCollection(params, api)
        .then(() => {
          console.log(`Was delete ${params.collectionName} collection`);
          return api.db.close({ client });
        })
        .then(() => {
          console.log("Success close connection");
          done();
        })
        .catch(err => {
          console.log(err);
          done();
        });
    });
  });
}

module.exports = test;
