const createIndex = require("../../../src/models/db/createIndex");
const writeData = require("../../../src/models/db/writeData");
const remove = require("../../../src/models/db/remove");
const dropCollection = require("../../../src/models/db/dropCollection");

function test(assert, api) {
  describe("Creates an index on the db and collection collection", () => {
    let client = {};
    let db = {};
    const collectionName = "pointLayer";
    const documents = [
      {
        properties: { code: "VS010-0000023-001-1019", flag: "testing" },
        geometry: {
          type: "Point",
          coordinates: [30.45937343620047, 59.724974062303296]
        }
      },
      {
        properties: { code: "VS010-0000023-001-1021", flag: "testing" },
        geometry: {
          type: "Point",
          coordinates: [30.4583300564727, 59.72510252474582]
        }
      }
    ];
    before(function before(done) {
      api.db
        .open({})
        .then(data => {
          client = data.client;
          db = data.db;
        })
        .then(() => {
          return writeData({ db, collectionName, documents }, api);
        })
        .then(res => {
          console.log(`Was write ${res.data.length} documents`);
          done();
        })
        .catch(err => {
          console.log(err);
          done();
        });
    });

    it("Success creating index for collection", done => {
      const params = {
        db,
        collectionName,
        query: { geometry: "2dsphere" }
      };
      createIndex(params, api)
        .then(response => {
          assert.typeOf(response, "object");
          assert.isTrue(response.status, "");
          assert.equal(
            response.statusText,
            `Индекс создан для коллекции ${collectionName}`
          );
          assert.equal(response.data, "geometry_2dsphere");

          done();
        })
        .catch(err => console.log(err));
    });

    it("Doesn't have working model db", done => {
      const params = {
        db: null,
        collectionName,
        query: { geometry: "2dsphere" }
      };
      createIndex(params, api)
        .then(response => {
          assert.typeOf(response, "object");
          assert.isFalse(response.status, "");
          assert.equal(response.statusText, "Нет рабочей модели db");

          done();
        })
        .catch(err => console.log(err));
    });

    it("Doesn't have field collectionName", done => {
      const params = {
        db,
        collectionName: "",
        query: { geometry: "2dsphere" }
      };
      createIndex(params, api)
        .then(response => {
          assert.typeOf(response, "object");
          assert.isFalse(response.status, "");
          assert.equal(response.statusText, "Нет collectionName");

          done();
        })
        .catch(err => console.log(err));
    });

    it("Doesn't have collection with name collectionName", done => {
      const params = {
        db,
        collectionName: "collectionName",
        query: { geometry: "2dsphere" }
      };
      createIndex(params, api)
        .then(response => {
          assert.typeOf(response, "object");
          assert.isTrue(response.status, "");

          assert.equal(response.data, "geometry_2dsphere");

          done();
        })
        .catch(err => console.log(err));
    });

    it("Doesn't have field query", done => {
      const params = {
        db,
        collectionName,
        query: null
      };
      createIndex(params, api)
        .then(response => {
          assert.typeOf(response, "object");
          assert.isFalse(response.status, "");
          assert.equal(response.statusText, "Нет query");
          done();
        })
        .catch(err => console.log(err));
    });

    after(function after(done) {
      const params = { db, collectionName: "collectionName" };
      dropCollection(params, api)
        .then(() => {
          console.log(`Was delete ${params.collectionName} collection`);
          return api.db.close({ client });
        })
        .then(() => {
          console.log("Success close connection");
          done();
        })
        .catch(err => {
          console.log(err);
          done();
        });
    });
  });
}

module.exports = test;
