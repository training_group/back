const testServicesLayer = require("./layer");
const testServicesFeatures = require("./features");

function testServices(assert, models) {
  return {
    // testServicesLayer: testServicesLayer(assert, models),
    testServicesFeatures: testServicesFeatures(assert, models)
  };
}

module.exports = testServices;
