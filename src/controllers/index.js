const layer = require("./layer");
const features = require("./features");

module.exports = function upGradedRouter(services, controller, logger) {
  layer(services.layer, controller, logger);
  features(services.features, controller, logger);
};
