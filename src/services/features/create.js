module.exports = function read(models) {
  return async params => {
    return firstFunction(params, models);
  };
};

async function firstFunction(params, models) {
  const { id, spatialReference, features } = params;
  if (!id) {
    return {
      status: false,
      statusText: "Не передали идентификатор слоя",
      data: {}
    };
  }

  if (!spatialReference || !spatialReference.latestWkid) {
    return {
      status: false,
      statusText: "Не передали идентификатор spatialReference",
      data: {}
    };
  }

  if (!features || !Array.isArray(features)) {
    return {
      status: false,
      statusText: `Ошибка в параметре feature = ${features}`,
      data: {}
    };
  }

  const projFeaturesPr = features.map(feature => {
    const _id = models.nanoid();
    return models.projection({
      feature: { ...feature, _id, properties: { ...feature.properties, _id } },
      sourceProj: spatialReference.latestWkid,
      targetProj: 4326
    });
  });

  const projFeatures = await Promise.all(projFeaturesPr)
    .then(values => {
      const items = values.reduce((acc, curr) => {
        if (curr.status) {
          return [...acc, curr.data];
        }
        return acc;
      }, []);
      return {
        status: true,
        data: items
      };
    })
    .catch(err => ({
      status: false,
      statusText: "Ошибка перепроецирования",
      data: err
    }));

  if (!projFeatures.status) {
    return projFeatures;
  }

  const response = await models.db.connect(params);
  if (!response.status) {
    return response;
  }

  const { client, db } = response.data;

  return models.db
    .writeData({
      db,
      collectionName: id,
      documents: projFeatures.data
    })
    .then(response => {
      models.db.disconnect({ client });
      return response;
    })
    .then(response => {
      const features = response.status ? response.data : [];
      return {
        status: true,
        statusText: `Объекты слоя ${id} успешно записаны`,
        data: {
          type: "FeatureCollection",
          crs: {
            type: "name",
            properties: {
              name: `EPSG:${spatialReference.latestWkid}`
            }
          },
          features: features
        }
      };
    })
    .catch(err => ({
      status: false,
      statusText: "Ошибка записи объектов в слой",
      data: err
    }));
}
