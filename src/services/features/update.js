module.exports = function update(models) {
  return async params => {
    return firstFunction(params, models);
  };
};

async function firstFunction(params, models) {
  const { id, spatialReference, features } = params;
  if (!id) {
    return {
      status: false,
      statusText: "Не передали идентификатор слоя",
      data: {}
    };
  }

  if (!spatialReference || !spatialReference.latestWkid) {
    return {
      status: false,
      statusText: "Не передали идентификатор spatialReference",
      data: {}
    };
  }

  if (!features || !Array.isArray(features)) {
    return {
      status: false,
      statusText: `Ошибка в параметре feature = ${features}`,
      data: {}
    };
  }

  const response = await models.db.connect(params);
  if (!response.status) {
    return response;
  }

  const { client, db } = response.data;

  const projFeaturesPr = features.map(feature => {
    return models.projection({
      feature,
      sourceProj: spatialReference.latestWkid,
      targetProj: 4326
    });
  });

  const projFeatures = await Promise.all(projFeaturesPr)
    .then(values => {
      const items = values.reduce((acc, curr) => {
        if (curr.status) {
          return [...acc, curr.data];
        }
        return acc;
      }, []);
      return {
        status: true,
        data: items
      };
    })
    .catch(err => ({
      status: false,
      statusText: "Ошибка перепроецирования",
      data: err
    }));

  if (!projFeatures.status) {
    return projFeatures;
  }

  let forInsert = [];

  const queue = projFeatures.data.reduce((acc, data) => {
    if (data.properties._id !== undefined) {
      acc.push(
        models.db.update({
          db,
          collectionName: id,
          query: {
            "properties._id": { $eq: data.properties._id }
          },
          data
        })
      );
    } else {
      forInsert.push(data);
    }
    return acc;
  }, []);

  if (forInsert.length !== 0) {
    forInsert = forInsert.map(feature => {
      const _id = models.nanoid();
      return { ...feature, _id, properties: { ...feature.properties, _id } };
    });
    queue.push(
      models.db.writeData({
        db,
        collectionName: id,
        documents: forInsert
      })
    );
  }

  return Promise.all(queue)
    .then(values => {
      const count = values.reduce((acc, current) => {
        if (current.status && typeof current.data === "number") {
          return acc + current.data;
        }
        if (current.status && typeof current.data === "object") {
          return acc + current.data.length;
        }

        return acc;
      }, 0);

      models.db.disconnect({ client });

      return {
        status: true,
        statusText: `Данные слоя ${id} обновлены`,
        data: count
      };
    })
    .catch(err => ({
      status: false,
      statusText: `Ошибка обновлени данных слоя ${id}`,
      data: err
    }));
}
