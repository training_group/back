const layer = require("./layer");
const features = require("./features");

module.exports = (models, config) => {
  return {
    layer: layer(models, config),
    features: features(models, config)
  };
};
