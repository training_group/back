const db = require("./db");

class Models {
  constructor(api) {
    this.api = api;
    this.db = db(api);
    this.projection = api.projection;
    this.nanoid = api.library.nanoid;
  }
}

module.exports = Models;
