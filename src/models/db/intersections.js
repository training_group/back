module.exports = async (params, api) => {
  return exportedFunction(params, api);
};

async function exportedFunction(params, api) {
  const { db, collectionName, locationField, targetObject } = params;

  api.logger.info(
    `Поиск пересечения объектов в ${collectionName} для целевого объекта`
  );

  if (!db || !db.collection) {
    return {
      status: false,
      statusText: "Нет рабочей модели db",
      data: {}
    };
  }
  if (collectionName === undefined) {
    return {
      status: false,
      statusText: "Нет collectionName",
      data: {}
    };
  }

  if (!locationField || typeof locationField !== "string") {
    return {
      status: false,
      statusText: "Нет locationField",
      data: {}
    };
  }

  const enumTypes = [
    "Point",
    "MultiPoint",
    "LineString",
    "MultiLineString",
    "Polygon",
    "MultiPolygon"
    // "GeometryCollection"
  ];

  if (
    !targetObject ||
    !targetObject.type ||
    !enumTypes.includes(targetObject.type) ||
    !targetObject.coordinates ||
    !Array.isArray(targetObject.coordinates) ||
    targetObject.coordinates.length === 0
  ) {
    return {
      status: false,
      statusText: "Нет валидного значения targetObject",
      data: {}
    };
  }

  try {
    const cursor = db.collection(`${collectionName}`).find({
      [locationField]: {
        $geoIntersects: {
          $geometry: targetObject
        }
      }
    });

    return cursor
      .toArray()
      .then(documents => {
        return {
          status: true,
          statusText: `Пересекаемые объекты`,
          data: documents
        };
      })
      .catch(err => {
        return {
          status: false,
          statusText: `Ошибка при выполнении пространственной операции`,
          data: err
        };
      });
  } catch (e) {
    return {
      status: false,
      statusText: `Ошибка создания cursor для collection ${collectionName}`,
      data: {}
    };
  }
}
