module.exports = async (params, api) => {
  return exportedFunction(params, api);
};

async function exportedFunction(params, api) {
  const { db, collectionName } = params;

  api.logger.info(`Удаление коллекции ${collectionName} из БД`);

  if (!db || !db.collection) {
    return {
      status: false,
      statusText: "Нет рабочей модели db",
      data: {}
    };
  }

  if (!collectionName) {
    return {
      status: false,
      statusText: "Нет collectionName",
      data: {}
    };
  }
  const collection = db.collection(`${collectionName}`);

  return collection
    .drop()
    .then(data => {
      return {
        status: true,
        statusText: `Коллекция ${collectionName} удалена`,
        data
      };
    })
    .catch(err => {
      api.logger.error(
        `Ошибка при удалении коллекции ${collectionName} из БД ${err}`
      );
      return {
        status: false,
        statusText: `Ошибка удаления коллекции ${collectionName}`,
        data: err
      };
    });
}
