const host = "localhost";

// Connection url
const url = `mongodb://${process.env.DB || "127.0.0.1"}:27017`;

// Database Name
const dbName = "test";

module.exports = {
  host,
  url,
  dbName
};
