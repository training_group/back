const banks = require("./data/banks");
const lines = require("./data/lines");
const polygon = require("./data/polygon");

const cls = require("cls-hooked");
const nanoid = require("nanoid");
const config = require("./src/config");
const logger = require("./middleware/logger");
const Models = require("./src/models");
let api = require("./src/api");
const services = require("./src/services");

api = api(config, logger);
const models = new Models(api);
const servicesLayer = services(models, config);

function setTraceId() {
  const clsNamespace = cls.createNamespace("app");
  const traceID = nanoid();
  clsNamespace.run(() => {
    clsNamespace.set("traceID", traceID);
  });
}

async function build() {
  setTraceId();
  const layers = {
    banks,
    lines,
    polygon
  };
  const keys = Object.keys(layers);
  for (let i = 0; i < keys.length; i++) {
    const layerName = keys[i];
    const { layer, data } = layers[layerName];
    const isCreateLayer = await servicesLayer.layer.create(layer);
    if (!isCreateLayer.status) {
      console.log(isCreateLayer);
      continue;
    }

    const isCreateFeatures = await servicesLayer.features.create(data);
    if (!isCreateFeatures.status) {
      console.log(isCreateFeatures);
    } else {
      console.log(`Успешно записан слой ${layer.name} и его данные`);
    }
  }
}

module.exports = build();
